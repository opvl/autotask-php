<?php

namespace ATWS\AutotaskObjects;

class Entity
{
    protected $fillable = [];
    protected $guarded = [];

    public function getFillable(){
        return $this->fillable;
    }

    public function getGuarded(){
        return $this->guarded;
    }

    public function __construct($attributes = [])
    {
        if (count($attributes)){
            foreach ($attributes as $key => $value) {
                if (array_search($key, $this->getGuarded()) || !array_search($key, $this->getFillable()))
                    continue;
                $this->$key = $value;
            }
        }
    }
    // Required
    public $id;
    public $Fields;
    public $UserDefinedFields;
}
